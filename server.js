//Dépandance
const express = require('express')
let app = require('express')()
const mysql = require('mysql2');
let bodyParser = require('body-parser')
const cookieParser = require("cookie-parser");
let sessions = require('express-session')
let Inscription = require('./private/model/inscription')


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


//midelwaare
app.use(cookieParser());
app.use(sessions({
    secret: 'mdpsecret',
    resave: false,
    saveUninitialized: true,
    // cookie: { secure: false } // true pour https
    cookie: { maxAge: 1000 * 60 * 60 * 24} // 24 heures
  }))


//model
app.use('/assets', express.static('public'))
app.set('view engine', 'ejs')




var pagepublic = require('./route/pagepublic');
app.use('/', pagepublic);

var pageco = require('./route/connexion');
app.use('/', pageco);


app.get('*', function(req, res){
    res.send(`La page que vous essayer d'atteindre n'existe pas, <a href=\'/'>cliquer ici </a> pour revenir à l'index`);
  });

//start
app.listen(8003)