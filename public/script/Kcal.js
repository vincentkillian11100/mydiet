genreH = document.getElementById('genreH')
genreF = document.getElementById('genreF')

poidKcal = document.getElementById('poidCalorie')
tailleKcal = document.getElementById('tailleCalorie')
ageKcal = document.getElementById('age')

sedentaire = document.getElementById('sedentaire')
actifLeger = document.getElementById('actifLeger')
actif = document.getElementById('actif')
actifPlus = document.getElementById('actifPlus')
calculEvent = document.getElementById('calculEvent')
valueKcal = document.getElementById('valueKcal')
resultatKcal = document.getElementById('resultatKcal')
sauvegardeKcal = document.getElementById('sauvegardeKcal')
date2 = document.getElementById('date2')



//Stockage provisoir des donner 
genreH.addEventListener('change', searchGenreKcal)
genreF.addEventListener('change', searchGenreKcal)
poidKcal.addEventListener('change', searchPoidsKcal)
tailleKcal.addEventListener('change', searchTailleKcal)
ageKcal.addEventListener('change', searchAgeKcal)

sedentaire.addEventListener('change', searchDepanceKcal)
actifLeger.addEventListener('change', searchDepanceKcal)
actif.addEventListener('change', searchDepanceKcal)
actifPlus.addEventListener('change', searchDepanceKcal)
calculEvent.addEventListener('click', masterKcal)


//J'ai creer plusieurs tabeau pour pouvoir stocker les valeur des inputs
//J'avais en tete d'enregistrer tout les donner d'entrer 
tabgenreKcal = []
tabPoidsKcal = []
tabTailleKcal = []
tabAgeKcal = []
tabDepanceKcal = []





console.log(tabgenreKcal);
console.log(tabPoidsKcal);
console.log(tabTailleKcal);
console.log(tabAgeKcal);
console.log(tabDepanceKcal);
// Récuperer les valeur des input en les mettant dans un tableau
function searchGenreKcal(e) {
    valGenreKcal = e.target.value;
    tabgenreKcal.push(valGenreKcal)
}
function searchPoidsKcal(e) {
    valPoidsKcal = e.target.value;
    tabPoidsKcal.push(valPoidsKcal)
}
function searchTailleKcal(e) {
    valTailleKcal = e.target.value;
    tabTailleKcal.push(valTailleKcal)
}
function searchAgeKcal(e) {
    valAgeKcal = e.target.value;
    tabAgeKcal.push(valAgeKcal)
}
function searchDepanceKcal(e) {
    valDepanceKcal = e.target.value;
    tabDepanceKcal.push(valDepanceKcal)
}



// Cette fonction est lancer lors de la demande de calcul
function masterKcal() {
    sauvegardeKcal.setAttribute('style','display:block!important;')
    let dat = new Date()
    let datt = dat.toLocaleDateString("fr")
    date2.value = datt;
    //si la valeur de l'input est égale a Femme et que la radio est cocher
    if (genreF.value == 'Femme' && genreF.checked) {
        //calculer les kilocalorie grace au tableau
        kcal = (9.740 * tabPoidsKcal[0]) + (172.9 * (tabTailleKcal[0] / 100)) - (4.737 * tabAgeKcal[0]) + 667.051;
        //si le tableau des dépance calorique est egale a sedentaire 
        if (tabDepanceKcal[0] = 'sedentaire') {
                    tabDepanceKcal[0] = tabDepanceKcal[0] * 1.2;
                    // On innerHTML un paragraphe contenant le résulat du calcul 
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    // On donnne la reponse du calcul dans la value de l'input qui va etre sauvegarder en bdd
                    valueKcal.value = kcal;
                }
                if (tabDepanceKcal[0] = 'actifLeger') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.375;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;

                }
                if (tabDepanceKcal[0] = 'actif') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.55;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;
                }
                if (tabDepanceKcal[0] = 'actifPlus') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.725;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;
                }
    }
    if (genreH.value == 'Homme' && genreH.checked) {
        //calculer les kilocalorie grace au tableau
        kcal = (13.707 * tabPoidsKcal[0]) + (492.3 * (tabTailleKcal[0] / 100)) - (6.673 * tabAgeKcal[0]) + 77.607;
        //si le tableau des dépance calorique est egale a sedentaire 
        if (tabDepanceKcal[0] = 'sedentaire') {
                    tabDepanceKcal[0] = tabDepanceKcal[0] * 1.2;
                    // On innerHTML un paragraphe contenant le résulat du calcul 
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    // On donnne la reponse du calcul dans la value de l'input qui va etre sauvegarder en bdd
                    valueKcal.value = kcal;
                }
                if (tabDepanceKcal[0] = 'actifLeger') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.375;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;

                }
                if (tabDepanceKcal[0] = 'actif') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.55;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;
                }
                if (tabDepanceKcal[0] = 'actifPlus') {
                    tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.725;
                    resultatKcal.innerHTML = `Votre score est de <span id='vert1'>${kcal}</span> attention au vent`;
                    valueKcal.value = kcal;
                }
    }
    
   
}


// if (genreF.value == 'Femme' && genreF.checked) {
//     kcal = (9, 740 * tabPoidsKcal[0]) + (492, 3 * tabTailleKcal[0]) - (6.673 * age);
//     console.log(kcal);
//     if (tabDepanceKcal[0] = 'sedentaire') {
//         tabDepanceKcal[0] = tabDepanceKcal[0] * 1.2;
//         return tabDepanceKcal[0]
//     }
//     if (tabDepanceKcal[0] = 'actifLeger') {
//         tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.375;
//         return tabDepanceKcal[0]
//     }
//     if (tabDepanceKcal[0] = 'actif') {
//         tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.55;
//         return tabDepanceKcal[0]
//     }
//     if (tabDepanceKcal[0] = 'actifPlus') {
//         tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.725;
//         return tabDepanceKcal[0];
//     }
// }
// if (genreH.value == 'Homme' && genreH.checked) {
//     kcal = ((13.707 * tabPoidsKcal[0]) + (492.3 * (tabTailleKcal[0]) / 100) - (6.673 * age));
//     console.log(kcal);
//     if (tabDepanceKcal[0] = 'sedentaire') {
//         tabDepanceKcal[0] = tabDepanceKcal[0] * 1.2;
//         return tabDepanceKcal[0];
//     }
//     if (tabDepanceKcal[0] = 'actifLeger') {
//         tabDepanceKcal[0] = tabDepanceKcal[0] * 1.375;
//         return tabDepanceKcal[0];
//     }
//     if (tabDepanceKcal[0] = 'actif') {
//         tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.55;
//         return tabDepanceKcal[0];
//     }
//     if (tabDepanceKcal[0] = 'actifPlus') {
//         tabDepanceKcal[0] = tabDepanceKcal[0]  * 1.725;
//         return tabDepanceKcal[0];
//     }
// }


