genreH2 = document.getElementById('genreHPub')
genreF2 = document.getElementById('genreFPub')

poidKcal2 = document.getElementById('poidCaloriePub')
tailleKcal2 = document.getElementById('tailleCaloriePub')
ageKcal2 = document.getElementById('agePub')

sedentaire2 = document.getElementById('sedentairePub')
actifLeger2 = document.getElementById('actifLegerPub')
actif2 = document.getElementById('actifPub')
actifPlus2 = document.getElementById('actifPlusPub')
calculEvent2 = document.getElementById('calculEventPub')


valueKcal2 = document.getElementById('valueKcalPub')


resultatKcal2 = document.getElementById('resultatKcalPub')//garder





//Stockage provisoir des donner 
genreH2.addEventListener('change', searchGenreKcal2)
genreF2.addEventListener('change', searchGenreKcal2)
poidKcal2.addEventListener('change', searchPoidsKcal2)
tailleKcal2.addEventListener('change', searchTailleKcal2)
ageKcal2.addEventListener('change', searchAgeKcal2)

sedentaire2.addEventListener('change', searchDepanceKcal2)
actifLeger2.addEventListener('change', searchDepanceKcal2)
actif2.addEventListener('change', searchDepanceKcal2)
actifPlus2.addEventListener('change', searchDepanceKcal2)
calculEvent2.addEventListener('click', masterKcal2)


//J'ai creer plusieurs tabeau pour pouvoir stocker les valeur des inputs
//J'avais en tete d'enregistrer tout les donner d'entrer 
tabgenreKcal2 = []
tabPoidsKcal2 = []
tabTailleKcal2 = []
tabAgeKcal2 = []
tabDepanceKcal2 = []




// Récuperer les valeur des input en les mettant dans un tableau
function searchGenreKcal2(e) {
    valGenreKcal2 = e.target.value;
    tabgenreKcal2.push(valGenreKcal2)
}
function searchPoidsKcal2(e) {
    valPoidsKcal2 = e.target.value;
    tabPoidsKcal2.push(valPoidsKcal2)
}
function searchTailleKcal2(e) {
    valTailleKcal2 = e.target.value;
    tabTailleKcal2.push(valTailleKcal2)
}
function searchAgeKcal2(e) {
    valAgeKcal2 = e.target.value;
    tabAgeKcal2.push(valAgeKcal2)
}
function searchDepanceKcal2(e) {
    valDepanceKcal2 = e.target.value;
    tabDepanceKcal2.push(valDepanceKcal2)
}



// Cette fonction est lancer lors de la demande de calcul
function masterKcal2() {
    //si la valeur de l'input est égale a Femme et que la radio est cocher
    if (genreF2.value == 'Femme' && genreF2.checked) {
        //calculer les kilocalorie grace au tableau
        kcal2 = (9.740 * tabPoidsKcal2[0]) + (172.9 * (tabTailleKcal2[0] / 100)) - (4.737 * tabAgeKcal2[0]) + 667.051;
        //si le tableau des dépance calorique est egale a sedentaire 
        if (tabDepanceKcal2[0] = 'sedentaire') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0] * 1.2;
                    // On innerHTML un paragraphe contenant le résulat du calcul 
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    // On donnne la reponse du calcul dans la value de l'input qui va etre sauvegarder en bdd
                    valueKcal2.value = kcal2;
                }
                if (tabDepanceKcal2[0] = 'actifLeger') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.375;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;

                }
                if (tabDepanceKcal2[0] = 'actif') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.55;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;
                }
                if (tabDepanceKcal2[0] = 'actifPlus') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.725;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;
                }
    }
    if (genreH2.value == 'Homme' && genreH2.checked) {
        //calculer les kilocalorie grace au tableau
        kcal2 = (13.707 * tabPoidsKcal2[0]) + (492.3 * (tabTailleKcal2[0] / 100)) - (6.673 * tabAgeKcal2[0]) + 77.607;
        //si le tableau des dépance calorique est egale a sedentaire 
        if (tabDepanceKcal2[0] = 'sedentaire') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0] * 1.2;
                    // On innerHTML un paragraphe contenant le résulat du calcul 
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    // On donnne la reponse du calcul dans la value de l'input qui va etre sauvegarder en bdd
                    valueKcal2.value = kcal2;
                }
                if (tabDepanceKcal2[0] = 'actifLeger') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.375;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;

                }
                if (tabDepanceKcal2[0] = 'actif') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.55;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;
                }
                if (tabDepanceKcal2[0] = 'actifPlus') {
                    tabDepanceKcal2[0] = tabDepanceKcal2[0]  * 1.725;
                    resultatKcal2.innerHTML = `Votre score est de <span id='vert1'>${kcal2}</span> attention au vent`;
                    valueKcal2.value = kcal2;
                }
    }
    
   
}