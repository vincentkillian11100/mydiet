var express = require('express');
const { updateinfos } = require('../private/model/Utilisateur');
var router = express.Router();

let Utilisateur = require('../private/model/Utilisateur')


var session;
let pingCount = 0;

router.get('/', function (req, res,) {
  res.render('connection')
})
router.get('/connection', function (req, res,) {
  session = req.session;
  console.log(session)
  if (req.session.user) {
    res.render('compteClient', { session: req.session })
  } else
    // res.sendFile('views/index.html',{root:__dirname})
    res.render('connection', { session: req.session });
});



router.post('/connection', function (req, res,) {
  session = req.session;

  Utilisateur.connection(req.body.identifiant, req.body.mdp,
    function (rows) {
      session.user = rows[0]
      if (rows.length > 0) {
        console.log(req.session.id);
        res.redirect('compteClient')
      }
      else {
        res.send("Vos identifiants sont incorrects, <a href=\'/connection'>cliquer pour revenir à la page de connection</a>");
      }

    });
});

router.get('/compteClient', function (req, res,) {
  session = req.session;
  if (req.session.user != undefined) {
    Utilisateur.selectInfo(req.session.user.id , function (result) {
      res.render('compteClient', { session: req.session, select: result })
    })
  } else {
    res.send("Vous devez vous connecter, <a href=\'/connection'>cliquer pour revenir à la page de connection</a>");
  }
});

router.post('/compteClient', function (req, res,) {
  session = req.session;
  console.log(req.body.id);
  Utilisateur.ArchivageInser(req.body.imc, session.user.id,req.body.date)
  res.redirect('compteClient')
});



router.get('/uptInfo', function (req, res,) {
  session = req.session;
  if (req.session.user) {
    res.render('compteClient', { session: req.session })
  } else
    res.render('connection', { session: req.session });
});

router.post('/uptInfo', function (req, res,) {
  session = req.session;
  Utilisateur.updateinfos(req.body.nom, req.body.prenom, req.body.mail, req.body.identifiant, req.body.mdp, session.user.id, function () {
    Utilisateur.connection(req.body.identifiant, req.body.mdp,
      function (rows) {
        session.user = rows[0]
        if (rows.length > 0) {
          console.log(req.session.id);
          res.redirect('compteClient')
        }
        else {
          res.send("Vos identifiants sont incorrects, <a href=\'/connection'>cliquer pour revenir à la page de connection</a>");
        }

      });

  })
});

router.post('/calculKcal', function (req, res,) {
  session = req.session;
  console.log(req.body.id);
  Utilisateur.CalculKcal(req.body.kcal, session.user.id,req.body.date)
  res.redirect('compteClient')

});

router.get('/deleteCompte', function (req, res,) {
  session = req.session;
  Utilisateur.deleteCompteHistorique(req.session.user.id, function () {
    Utilisateur.deleteCompteUtilisateur(req.session.user.id)
  })
})






router.get('/deco', (req, res) => {
  req.session.destroy();
  res.redirect('/connection');
});



module.exports = router;
