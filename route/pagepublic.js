
let express = require('express');
let router = express.Router();


let Utilisateur = require('../private/model/Utilisateur')
const { render } = require('ejs');
var session;


let secureInputs = data => {
  Object.keys(data).forEach(key => data[key] = data[key].replace(/<.*?>|select|from|where|update|insert/g  , ""));
}


router.get('/inscription', function (req, res,) {
  res.render('inscription')
})

router.post('/inscription', function (req, res,) {
    session = req.session;
    secureInputs(req.body)
    console.log(req.body);
    Utilisateur.verifCompte(req.body.identifiant,  function(rows){
      if(rows.length > 0){
        res.send("Vous posèdez deja un compte, <a href=\'/connection'>cliquer pour revenir à la page de connection</a>");
        
      }else{
        Utilisateur.register(req.body.genre, req.body.nom, req.body.prenom, req.body.age,req.body.mail, req.body.identifiant, req.body.mdp)
        res.redirect('/')
         
      }
    })
});

module.exports = router;


